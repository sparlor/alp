$(document).ready(function() {
if ($(window).width() < 981) {
    $('.navbar-center').addClass('active-center');
    $('.container-fluid.content').css({"top":57});
    $(".nav>li>a").removeClass("links");
    $('.navbar-fixed-top').attr("height","57");
    $('.navbar-center').attr({
        "padding-top" : 0,
        "top" : -23,
    });
    $('.logo').addClass("mobile-logo");
    $(".nav>li>a:not(:last)").hide();
    $('a.ham-icon').css("font-size","35px");
    $('.active-center').css("-webkit-transition","0s");
    $('.navbar-center').css("margin-left","0px");
    $('.active-center').css("top", "-27px");
    $('.active-center').css("right", "100px");


 }
 else {
    $('.navbar-fixed-top').attr("height", "180px");
    $('.navbar-center').attr({
        "padding-top" : 200,
        "margin-left" : 374,
    });
 }
});
//import { or } from "ip";

$(function() {
  if ($(window).width() > 981) {
  $(window).on("scroll", function() {
      if($(window).scrollTop() > 50) {
          $(".navbar-fixed-top").addClass("active");
          $(".logo").addClass("active-logo");
          $(".nav>li>a").addClass("links");
          $('.navbar-center').addClass("active-center");
          $('.container.header').addClass("active-header");
          $('.container-fluid.content').addClass("active-content");
          $(".slogan").addClass("active-slogan");
          $('.ham-icon').show();
          $(".links:not(:last)").hide();
          $(".container-fluid.content").attr("top:0px");


      } else {
          //remove the background property so it comes transparent again (defined in your css)
         $(".navbar-fixed-top").removeClass("active");
         $(".logo").removeClass("active-logo");
         $(".nav>li>a").removeClass("links");
         $(".navbar-center").removeClass("active-center");
         $('.container.header').removeClass("active-header");
         $('.container-fluid.content').removeClass("active-content");
         $(".slogan").removeClass("active-slogan");
         $('.ham-icon').hide();
         $(".nav>li>a:not(:last)").show();
      }
    });
  };

});

// Trigger CSS animations on scroll.
// Detailed explanation can be found at http://www.bram.us/2013/11/20/scroll-animations/

// Looking for a version that also reverses the animation when
// elements scroll below the fold again?
// --> Check https://codepen.io/bramus/pen/vKpjNP


$(function($) {
  
  // Function which adds the 'animated' class to any '.animatable' in view
  var doAnimations = function() {
    
    // Calc current offset and get all animatables
    var offset = $(window).scrollTop() + $(window).height(),
        $animatables = $('.animatable');
    
    // Unbind scroll handler if we have no animatables
    if ($animatables.length == 0) {
      $(window).off('scroll', doAnimations);
    }
    
    // Check all animatables and animate them if necessary
		$animatables.each(function(i) {
       var $animatable = $(this);
			if (($animatable.offset().top + $animatable.height() - 20) < offset) {
        $animatable.removeClass('animatable').addClass('animated');
			}
    });

	};
  
  // Hook doAnimations on scroll, and trigger a scroll
	$(window).on('scroll', doAnimations);
  $(window).trigger('scroll');

});

$(document).ready(function() {
  $('.carousel').carousel({interval: 3000});

  function myFunction() {
    var x = document.getElementById("main-nav");
    if (x.className === "navbar-center") {
        x.className += "active-center";
    } else {
        x.className = "navbar-center";
    }
  }
});
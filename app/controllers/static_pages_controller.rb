class StaticPagesController < ApplicationController
  def Home
  end

  def Galleries
  end

  def About
  end

  def Contact
  end
end

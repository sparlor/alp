Rails.application.routes.draw do

  root 'static_pages#Home'

  get 'galleries' => 'static_pages#Galleries'

  get 'about' => 'static_pages#About'

  get 'contact' => 'static_pages#Contact'

  get 'investment' => 'static_pages#Investment'

  get 'blog' => 'static_pages#Blog'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
